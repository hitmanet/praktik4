import { favoriteMovies } from '../../helpers'
import './style.css'



export default Vue.component('film', {
    props: {
        film: Object,
        filmId: String
    },
    data: () => ({
        isFavorite: null
    }),
    template: `
        <div>
            <div class="film-card__header">
                <h3>{{ film.Title }}</h3>
                <span>{{ film.Year }}</span>
                <button @click="$emit('change-state')" class="back">Back</button>
                <button @click="changeIsFavorite" class="favorite-btn">{{ isFavorite ? 'Delete from favorite' : 'Add from favorite' }}</button>
            </div>
            <div class="film-card__info">
                <img :src="film.Poster !== 'N/A' ? film.Poster : './noimg.jpg'" alt="No poster">
                <span>Genre: {{ film.Genre }}</span>
                <span>Actors:{{ film.Actors }}</span>
                <span>Plot: {{ film.Plot }}</span>
                <span>Runtime: {{ film.Runtime }}</span>
                <span>ImdbRating: {{ film.imdbRating }}</span>
            </div>
        </div>
    `,
    mounted() {
        this.isFavorite = Boolean(favoriteMovies.movies.find(film => film.imdbID === this.filmId))
    },
    methods: {
        changeIsFavorite(){
            if (!this.isFavorite){
                favoriteMovies.movies.push(this.film)
            } else {
                const currentFilmIndex = favoriteMovies.movies.findIndex(film => {
                    return film.imdbID === this.filmId
                })
                favoriteMovies.movies.splice(currentFilmIndex, 1)
            }
            localStorage.setItem('favorite', JSON.stringify(favoriteMovies))
            this.isFavorite = !this.isFavorite
            this.$emit('updated')
        }
    },
})
