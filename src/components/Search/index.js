import { Api } from '../../services/Api'

export default Vue.component('search', {
    data: () => ({
        searchInput: ''
    }),
    methods: {
        async search(){
            const films = await Api.getFilmsByQuery(`?s=${this.searchInput}`)
            if (films.Error){
                // document.querySelector('.movies').innerHTML = 'Not found'
            } else {
                console.log(this.$parent)
                this.$emit('films', films)
            }
        }
    },
    template: `
    <div class="search">
        <input type="text" v-model="searchInput">
        <button @click="search">Search</button>
    </div>
    `
})