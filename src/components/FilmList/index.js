import { favoriteMovies } from '../../helpers'
export default Vue.component('filmlist', {
    template: `
        <div>
            <h2>{{ title }}</h2>
            <div v-for="(film, key) in films" :key="key">
                <img :src = "film.Poster !== 'N/A' ? film.Poster : './noimg.jpg'" alt = 'No film poster'>
                <span class="film__title">{{ film.Title }}</span>
                <span class="film__year">{{ film.Year }}</span>
                <button @click="stateEmit(film.imdbID)" class="film__link">Learn more</button>
                <button @click="changeIsFavorite(film.imdbID, film)">{{ isFavorite(film.imdbID) ? 'Delete from favorite' : 'Add to favorite' }}</button>
            </div>
        </div>
    `,
    props: {
        films: Array,
        title: String
    },
    methods: {
        stateEmit(filmId){
            this.$emit('film', filmId)
        },
        isFavorite(filmId){
            console.log(this.films)
            return Boolean(favoriteMovies.movies.find(film => film.imdbID === filmId))
        },
        //TODO: Вынести логику в отдельный компонент и кастомизировать film
        changeIsFavorite(filmId, film){
            if (!this.isFavorite(filmId)){
                favoriteMovies.movies.push(film)
            } else {
                const currentFilmIndex = favoriteMovies.movies.findIndex(film => {
                    return film.imdbID === filmId
                })
                favoriteMovies.movies.splice(currentFilmIndex, 1)
            }
            localStorage.setItem('favorite', JSON.stringify(favoriteMovies))
            this.$forceUpdate()
            this.$emit('updated')
        }
    }
})