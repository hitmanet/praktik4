import "@babel/polyfill"
import App  from './App'

new Vue({
    el: '#app',
    components: { App },
})