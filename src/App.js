import Search from './components/Search/index'
import FilmList from './components/FilmList/index'
import Film from './components/Film/index'
import { Api } from './services/Api'

export default Vue.component('app', {
    data: () => ({
        films: null,
        state: {
            mode: 'list'
        },
        favorite: null,
        oneFilmId: null,
        film: null
    }),
    components: { Search, FilmList, Film },
    template: `
        <div>
            <search v-if="state.mode === 'list'" @films="setFilms"></search>
            <filmlist @film="(filmId) => getFilm(filmId)" @updated="() => updated()" title="Favorite" v-if="favorite && state.mode === 'list'" :films="favorite.movies"></filmlist>
            <filmlist @film="(filmId) => getFilm(filmId)" @updated="() => updated()" title="Films" v-if="films && state.mode === 'list'" :films="films"></filmlist>
            <film @change-state="() => updated()" v-if="film && state.mode === 'film'" :filmId="oneFilmId" :film="film"></film>
        </div>
    `,
    methods: {
        setFilms(films){
            this.films = films
        },
        async getFilm(filmId){
            this.film = await Api.getFilmsByQuery(`?i=${filmId}`)
            this.oneFilmId = filmId
            this.state.mode = 'film'
        },
        updated(){
            this.getIsFavorite()
            this.state.mode = 'list'
        },
        getIsFavorite(){
            this.favorite = JSON.parse(localStorage.getItem('favorite')) || { movies: [] }
        }
    },
    mounted() {
        this.getIsFavorite()
    },
})