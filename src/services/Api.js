const url = 'http://www.omdbapi.com/'

export class Api {
    static async getFilmsByQuery(query){
        try{
            const response = await fetch(`${url}/${query}&apikey=d5677312`)
            const jsonResponce = await response.json()
            return jsonResponce.Search || jsonResponce
        } catch (error){
            console.log(error)
        }

    }
}